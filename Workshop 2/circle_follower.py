#!/usr/bin/env python
import sys
import RPi.GPIO as GPIO
import serial
import time
from collections import Counter
import math
import arduinoCom
import os

#Variable to enable or disable code on Pi, this variable can be triggered from switch later.
Run = True
#Pin configuration type BCM
GPIO.setmode(GPIO.BCM)

#Close Serial Connections to Raspberry Pi
def Close_connection():
    GPIO.cleanup()
    robot.closeSerial()
    sys.exit('Empty Sear Found - Task finished')

#Create an Arduino Object which has base code for communication between Pi and Arduino
arduino = arduinoCom.Arduino()

time.sleep(2)

#Try connecting to Ardunio from any of the USB ports
for i in range (10):
    if arduino.connect() == 1:
        print 'connection established'
        break
    else:
        print 'Connection failed'

from picamera.array import PiRGBArray
from picamera import PiCamera
import time
import cv2
import numpy as np
import cv2.cv as cv


def findColor( hsv_image, center ):
	thresh_blue = cv2.inRange(hsv_image, BLUE_MIN, BLUE_MAX)
	if (thresh_blue[center[0]][center[1]] == 255):
		return "b"
	thresh_red = cv2.inRange(hsv_image, RED_MIN, RED_MAX)
	if (thresh_red[center[0]][center[1]] == 255):
		return "r"
	thresh_green = cv2.inRange(hsv_image, GREEN_MIN, GREEN_MAX)
	if (thresh_green[center[0]][center[1]] == 255):
		return "g"
	#circleColor = "Green"


# initialize the camera and grab a reference to the raw camera capture
img_rows = 240
img_cols = 320
camera = PiCamera()
camera.awb_mode = 'off'
camera.awb_gains = (1.3, 1.7)
camera.resolution = (img_rows, img_cols)
camera.framerate = 32
rawCapture = PiRGBArray(camera, size=(img_rows, img_cols))

RED_MIN = np.array([0, 66, 138], np.uint8)
GREEN_MIN = np.array([48, 144, 41], np.uint8)
BLUE_MIN = np.array([70, 82, 73], np.uint8)
RED_MAX = np.array([10, 253, 241], np.uint8)
GREEN_MAX = np.array([63, 255, 255], np.uint8)
BLUE_MAX = np.array([142, 204, 255], np.uint8)

# allow the camera to warmup
time.sleep(0.1)

# capture frames from the camera
for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
    # grab the raw NumPy array representing the image, then initialize the timestamp
    # and occupied/unoccupied text
    image = frame.array

    # show the frame
    hsv_image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    circles = cv2.HoughCircles(gray_image, cv.CV_HOUGH_GRADIENT, dp=2, minDist=60, minRadius = 50, maxRadius=80)
    valid_circle = False
    if circles is not None:
		for i in range(0, len(circles[0])):
			#circle_center = ()
			if circles[0][i][0] <= img_cols and circles[0][i][1] <= img_rows:
				color = findColor(hsv_image, circles[0][i])
				if color is not None:
                                        valid_circle = True
					if (color == "r"):
						lineColor = (0, 0, 255)
					if (color == "g"):
						lineColor = (0, 255, 0)
					if (color == "b"):
						lineColor = (255, 0, 0)
					print(findColor(hsv_image, circles[0][i]))
					print circles[0][i]
					if circles[0][i][0] <= img_cols/2:
                                            print("go left")
                                            arduino.set_value(0, 50)
                                            #time.sleep(0.1)
					else:
                                            print("go right")
                                            arduino.set_value(0, -50)
                                            #time.sleep(0.1)
					
					cv2.circle(image, (circles[0][i][0],circles[0][i][1]), circles[0][i][2], lineColor)

    if valid_circle == False:
        arduino.set_value(0, 0)
        time.sleep(0.1)
#			else:
#				print("Circle coordinates out of image bounds")
#				print circles[0][i]
#				print("\n")

        
    cv2.imshow("Frame", image)
    
    
    key = cv2.waitKey(1) & 0xFF

    # clear the stream in preparation for the next frame
    rawCapture.truncate(0)

    # if the `q` key was pressed, break from the loop
    if key == ord("q"):
        break


#Sample code for using the set get protocol
#while True:
#    cmd = input("Give an input in following format: linear_velocity, angular_velocity, time_in_seconds: ")
#    cmd_time = time.time()
#    print cmd[0]
#    print cmd[1]

#    while (time.time()-cmd_time)<cmd[2]:
#		arduino.set_value(cmd[0],cmd[1])
#		time.sleep(0.1)
