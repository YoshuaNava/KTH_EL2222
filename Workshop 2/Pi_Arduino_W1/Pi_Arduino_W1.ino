#include "motors.h"
#include <QTRSensors.h>

// QTR8RC stuff (IR sensor array)
#define NUM_SENSORS   8     // number of sensors used
#define TIMEOUT       2500  // waits for 2500 microseconds for sensor outputs to go low
#define EMITTER_PIN   2     // emitter is controlled by digital pin 2

// Sensors 0 through 7 are connected to digital pins 3 through 10, respectively
QTRSensorsRC qtrrc((unsigned char[]) { 3, 4, 5, 6, 7, 8, 9, 10 },
	NUM_SENSORS, TIMEOUT, EMITTER_PIN);
unsigned int sensorValues[NUM_SENSORS];
// Velocity and position variables
int position, V0, VL, VR, Vmin, Vmax, Vamp;
// Gain of the line-following control loop
float P;


//384 ticks per circle
const double DEG_PER_TIC = (double)360 / (double)384;
const double WHEEL_RADIUS = 0.06425 / 2;
const double ROBOT_BASE = 0.13;
unsigned int left_enc_count, right_enc_count;
unsigned long time_prev, time_now;
double left_angular_vel, right_angular_vel;
//Create motor objects with connections and  parameters
//Arguments: encoder pin, to motor : out1, out2, enable pin,inverse direction,Kp,Ki,Kd
Motor left_motor(3, 10, 11, 6, false, 4.0, 0, 2.0);
Motor right_motor(2, 8, 9, 5, true, 6.0, 0, 2.5);



void setup()
{
    delay(500);
    //Serial communication initialization
    Serial.begin(9600);
    
    //Configure interrupt pins for encoders
    attachInterrupt(digitalPinToInterrupt(left_motor.ENCODER_PIN), left_tic_counter , CHANGE);
    attachInterrupt(digitalPinToInterrupt(right_motor.ENCODER_PIN), right_tic_counter , CHANGE);
    
    // Specify lowest and highest PWM values that the motors function
    Vmin = 30;
    Vmax = 255;
    // Specify desired straight-line speed of the robot (PWM range: Vmin - 255)
    V0 = 100;
    // Find maximum permisible amplitude of velocity command (around V0)
    Vamp = V0 - Vmin;
    // Specify gain of line-following algorithm (P range: 0.0 - 1.0)
    P = 1;
    
    // Calibrate all sensors to light conditions
	for (int i = 0; i < 400; i++)  // make the calibration last about 10 seconds
	{
		qtrrc.calibrate();         // reads all sensors 10 times at 2500 us per read (i.e. ~25 ms per call)
	}
	
	delay(1000);
    
}


void loop()
{
    
    // Read calibrated sensor values and obtain a measure of the line position from 0 to 7000
	// To get raw sensor values, call:
	// qtrrc.read(sensorValues); instead of unsigned int position = qtrrc.readLine(sensorValues);
	// Map position with respect to center of sensor array
	position = map(qtrrc.readLine(sensorValues), 0, 7000, -Vamp, Vamp);

	// Calculate differential drive speed of each wheel
	if (position >= 0)
	{
		VR = V0-(int)(P*position);
		VL = V0+(int)(P*position);
	}
	else
	{
		VR = V0+(int)(P*position);
		VL = V0-(int)(P*position);
	}
	
	// Send motor command
	right_motor.rotate(VR, false);
	left_motor.rotate(VL, false);

	delay(50);

}


//Callback functions when interrupt are triggered by encoders
void left_tic_counter() 
{
  //call motor tick counter
  left_motor.update_ticks();
}



void right_tic_counter()
{
  //call motor tick counter
  right_motor.update_ticks();
}

